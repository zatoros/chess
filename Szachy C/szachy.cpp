#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<stdlib.h>
#include"figury.h"

#define PRZECIWNIK  GRACZ Przeciwnik = ( Gracz == BIALE ? CZARNE : BIALE );
#define X1 x%8+96+1
#define X2 x/8+1
#define Y1 y%8+96+1
#define Y2 y/8+1
void usun()
{
	while((getchar())!='\n')
		continue;
}

int getm()
{
	int i;
		while((scanf("%d",&i))!=1 || !( i>=1 &&  i<=2))
		{
			printf("Zla liczba sproboj jeszcze raz\n");
			usun();
		}
		usun();
	return  i;
}

int geta()
{
	int i;
		while((scanf("%d",&i))!=1 || !( i>=1 &&  i<=4))
		{
			printf("Zla liczba sproboj jeszcze raz\n");
			usun();
		}
		usun();
	return  i;
}

int getx()
{
	char i;
		while((scanf("%c",&i))!=1 || !( i>='a' &&  i<='h'))
		{
			printf("Zla wspolzendna sproboj jeszcze raz\n");
			usun();
		}
		usun();
	return  i;
}

int gety()
{
	int i;
		while((scanf("%d",&i))!=1 || !(i>=1 && i<=8))
		{
			printf("Zla wspolzendna sproboj jeszcze raz\n");
			usun();
		}
		usun();
	return i;
}
			
int przelicz(int a, int b)
{
	a-=96;
	
	if( a>=1 && a<=8 && b>=1 && b<=8 )
	return (b-1)*8+(a-1);
	return -1;
}

void pokaz_plansze(PLANSZA Plansza,GRACZ Gracz)				
{
	int i,j;
	
	printf("   ");
	for(j='a';j<='h';j++)
		printf("%c ",(char) j);
		putchar('\n');
	if(Gracz == BIALE)
		for(i=8;i>=1;i--)
			{
				printf("%d  ",i);
				for(j='a';j<='h';j++)
					printf("%c ",(char) plansza(przelicz(j,i),Plansza));
				printf("  %d\n",i);
			}
	else
		for(i=1;i<=8;i++)
			{
				printf("%d  ",i);
				for(j='a';j<='h';j++)
					printf("%c ",(char) plansza(przelicz(j,i),Plansza));
				printf("  %d\n",i);
			}
	printf("   ");
	for(j='a';j<='h';j++)
		printf("%c ",(char) j);
		putchar('\n');
}

bool start(STAN * Stan)
{
	if(*Stan !=STOP)
		return false;
	
	*Stan=RUCH;
		return true;
}

bool koniec(STAN * Stan)
{
	if(*Stan == STOP)
		return false;
		
	*Stan = STOP;
		return true;
}

void start_gry(STAN* Stan)
{
		if(start(Stan))
		printf("Gra rozpoczeta\n");
		
		else 
		{
		fprintf(stderr,"Blad, nie da sie rozpoczac gry\n");
		exit(1);
		}
}

void koniec_gry(STAN * Stan)
{
	if(koniec(Stan))
	{
	printf("Koniec gry\n");
	printf("Nacisnij dowolny klawisz by kontynuowac\n");
	getchar();
	}
	else
	{
	fprintf(stderr,"Blad nie da sie skonczyc gry\n");
	exit(1);
	}
}

void pobierzxy(int *x)
{
	printf("Podaj wspolzendna literowa\n");
		x[0]=getx();
	printf("Podaj wspolznedna liczbowa\n");
		x[1]=gety();
}

bool czy_puste(POLE Pole)							
{
	if(Pole==PUSTE)
			return true;
	return false;
}

bool czy_biale(POLE Pole)							
{
	if(isupper( (char) Pole ) && !czy_puste(Pole))
		return true;
	return false;
}

bool czy_czarne(POLE Pole)							
{
	if(islower ( (char) Pole) && !czy_puste(Pole))
		return true;
	return false;
}

bool czy_gracza(POLE Pole,GRACZ Gracz)
{
	if( ( Gracz ==BIALE && czy_biale(Pole) ) || ( Gracz == CZARNE && czy_czarne(Pole)) )
		return true;
	return false;
}

bool ruch(int x, int y, PLANSZA Plansza, STAN Stan, GRACZ Gracz)
{
	bool figurka;
	PRZECIWNIK

	if(Stan != RUCH && Stan !=SZACH)
			return false;
		
	if(czy_puste(plansza(x,Plansza)))							
			return false;
			
	if(czy_gracza(plansza(x,Plansza),Przeciwnik))				
			return false;
	if(czy_gracza(plansza(y,Plansza),Gracz))					
			return false;

			

			figurka = ruch_figury(x,y,Plansza,Gracz);
	if(figurka == false)
			return false;
		
	return true;
}

bool rysuj_gre(PLANSZA Plansza, STAN *Stan,GRACZ *Gracz)
{
	
		if(*Stan == STOP)
			return false;
			
	pokaz_plansze(Plansza,*Gracz);
		
		if(*Stan == SZACH)
			printf("SZACH\n");
			
		else  if(*Stan == PAT)
		{
			printf("PAT\n");
			return true;
		}
		
		else if(*Stan == MAT)
		{
			printf("SZACH MAT\n");
			printf("Wygraly %s\n", (*Gracz==BIALE ? "Biale" : "Czarne"));
			return true;
		}
		
	printf("Ruszaja sie %s\n", (*Gracz==BIALE ? "Biale" : "Czarne"));
	return true;
}

void pobiez_ruch(int *from,int *to)
{
	printf("Podaj numer pola figury ktora chcesz sie ruszyc\n");
	pobierzxy(from);
	printf("Podaj numer pola na ktore chcesz wskoczyc\n");
	pobierzxy(to);
}

bool tura(int *from, int *to, PLANSZA Plansza, STAN *Stan, GRACZ *Gracz)
{
	int x = przelicz(from[0],from[1]);
	int y = przelicz(to[0],to[1]);
	GRACZ Przeciwnik = (*Gracz == BIALE ? CZARNE : BIALE);
	
	if(!ruch(x,y,Plansza,*Stan,*Gracz))
		{
			printf("Ten ruch jest niedozwolony\n");
			return false;
		}
		
	if(czy_bedzie_szach(x,y,Plansza,*Gracz)) 		
		{
			if(*Stan == RUCH)
				printf("Twoj ruch powoduje u ciebie szacha\n");
			else
				printf("Masz szacha\n");
			return false;
		}
		
	rusz_sie(x,y,Plansza,RZECZYWISTY);						
		
	
	if(czy_szach(Plansza,Przeciwnik))				
	{
		(Przeciwnik == BIALE) ? roszada( przelicz('e',1) ,UAKTUALNIJ,Plansza) : roszada(przelicz('e',8),UAKTUALNIJ,Plansza);
		if(czy_mat(Plansza,Przeciwnik))			
			{
				*Stan=MAT;
				return true;
			}
		*Stan=SZACH;
	}
	
	else if(czy_pat(Plansza,Przeciwnik))		
	{
		*Stan=PAT;
		return true;
	}
	else
		*Stan = RUCH;
		
	*Gracz = (*Gracz == BIALE ? CZARNE : BIALE );
	return true;
}

bool czy_bedzie_szach(int x,int y,PLANSZA Plansza,GRACZ Gracz)						
{	
	bool szach=false;
	PLANSZA Kopia;
	inicjuj_plansze(&Kopia);
		kopiuj_plansze(Kopia,Plansza);
	rusz_sie(x,y,Kopia,WIRTUALNY);
		if(czy_szach(Kopia,Gracz))
		szach=true; 	 
		usun_plansze(&Kopia);
		return szach;
}

bool czy_szach(PLANSZA Plansza,GRACZ Gracz)					
{
	int i,x;
	GRACZ Przeciwnik;
		for(i=0;i<64;i++)
			if((Gracz == BIALE && plansza(i,Plansza) == KROLb) || (Gracz == CZARNE && plansza(i,Plansza) == KROLc))
				{
					x=i;
					break;
				}
	Przeciwnik = (plansza(x,Plansza) == KROLb ? CZARNE : BIALE);	
	
		for(i=0;i<64;i++)
		{
			if(i==x)
				continue;
			if(ruch(i,x,Plansza,RUCH,Przeciwnik))
				return true;
		}
	return false;
}

bool ruch_pionek(int x, int y, PLANSZA Plansza, GRACZ Gracz)				
{

	if(Gracz == BIALE) 
	{
		if(czy_puste(plansza(y,Plansza)))
		{
			if(y==przelicz(X1,X2+1) )
				return true;
			else if	((x>= przelicz('a',2) && x<= przelicz('h',2) ) && y==przelicz(X1,X2+2))
				return true;
		}
		else if(czy_czarne(plansza(y,Plansza)))
		{
			if( y==przelicz(X1+1,X2+1) || y==przelicz(X1-1,X2+1))
				return true;
		}
	}
	else
	{
		if(czy_puste(plansza(y,Plansza)))
		{
			if(y==przelicz(X1,X2-1) )
				return true;
			else if	((x>= przelicz('a',7) && x<= przelicz('h',7) ) && y==przelicz(X1,X2-2))
				return true;
		}
		else if(czy_biale(plansza(y,Plansza)))
		{
			if( y==przelicz( X1+1,X2 -1) || y==przelicz(X1-1,X2-1))
				return true;
		}
	}
	return false;
}

bool ruch_wieza(int x, int y, PLANSZA Plansza, GRACZ Gracz)				
{
	int i, j;
	bool flag;
	
	for(flag=true,i=1;i<8;i++)
		{	j=przelicz(X1+i,X2);
			if( y == j )
				break;
			if( j!= -1 && !czy_puste(plansza(j,Plansza)))
				flag=false;
		}
	if(i!=8 && flag == true)
		return true;
		
		for(flag=true,i=1;i<8;i++)
		{	j=przelicz(X1-i,X2);
			if( y == j )
				break;
			if( j!= -1 && !czy_puste(plansza(j,Plansza)))
				flag=false;
		}
	if(i!=8 && flag == true)
		return true;
		
		for(flag=true,i=1;i<8;i++)
		{	j=przelicz(X1,X2+i);
			if( y == j )
				break;
			if( j!= -1 && !czy_puste(plansza(j,Plansza)))
				flag=false;
		}
	if(i!=8 && flag == true)
		return true;
		
		for(flag=true,i=1;i<8;i++)
		{	j=przelicz(X1,X2-i);
			if( y == j )
				break;
			if( j!= -1 && !czy_puste(plansza(j,Plansza)))
				flag=false;
		}
	if(i!=8 && flag == true)
		return true;
		
		return false;

}

bool ruch_goniec(int x,int y,PLANSZA Plansza,GRACZ Gracz)				
{
	int i, j;
	bool flag;
	
	for(flag=true,i=1;i<8;i++)
		{	j=przelicz(X1+i,X2+i);
			if( y == j )
				break;
			if( j!= -1 && !czy_puste(plansza(j,Plansza)))
				flag=false;
		}
	if(i!=8 && flag == true)
		return true;
		
		for(flag=true,i=1;i<8;i++)
		{	j=przelicz(X1-i,X2+i);
			if( y == j )
				break;
			if( j!= -1 && !czy_puste(plansza(j,Plansza)))
				flag=false;
		}
	if(i!=8 && flag == true)
		return true;
		
		for(flag=true,i=1;i<8;i++)
		{	j=przelicz(X1+i,X2-i);
			if( y == j )
				break;
			if( j!= -1 && !czy_puste(plansza(j,Plansza)))
				flag=false;
		}
	if(i!=8 && flag == true)
		return true;
		
		for(flag=true,i=1;i<8;i++)
		{	j=przelicz(X1-i,X2-i);
			if( y == j )
				break;
			if( j!= -1 && !czy_puste(plansza(j,Plansza)))
				flag=false;
		}
	if(i!=8 && flag == true)
		return true;
		
		return false;
}

bool ruch_krolowa(int x,int y,PLANSZA Plansza,GRACZ Gracz)	
{
	if(ruch_goniec(x,y,Plansza,Gracz) || ruch_wieza(x,y,Plansza,Gracz))
		return true;
	return false;
}

bool ruch_skoczek(int x,int y,PLANSZA Plansza,GRACZ Gracz)
{
	if(y == przelicz(X1+2,X2+1) || y == przelicz(X1+2,X2-1) || y == przelicz(X1-2,X2+1) || y == przelicz(X1-2,X2-1) || 
		y == przelicz(X1+1,X2+2) || y == przelicz(X1-1,X2+2) || y == przelicz(X1-1,X2-2) || y == przelicz(X1+1,X2-2) )
		return true;
	return false;
}

bool ruch_krol(int x,int y, PLANSZA Plansza, GRACZ Gracz)		
{	
		if((y == przelicz(X1+1,X2) || y == przelicz(X1-1,X2) || y == przelicz(X1,X2+1) || y == przelicz(X1,X2-1) || 
		y == przelicz(X1+1,X2+1) || y == przelicz(X1-1,X2+1) || y == przelicz(X1-1,X2-1) || y == przelicz(X1+1,X2-1)) &&
		(przelicz(Y1+1,Y2)==-1 || !czy_krol(plansza(y+1,Plansza)) || x==y+1)  &&
		(przelicz(Y1-1,Y2)==-1 || (!czy_krol(plansza(y-1,Plansza)) || x==y-1) ) &&
		(przelicz(Y1+1,Y2+1)==-1 || (!czy_krol(plansza(y+9,Plansza)) || x==y+9) ) &&
		(przelicz(Y1+1,Y2-1)==-1 || (!czy_krol(plansza(y-7,Plansza)) || x==y-7) ) &&
		(przelicz(Y1,Y2+1)==-1 || (!czy_krol(plansza(y+8,Plansza)) || x==y+8) ) &&
		(przelicz(Y1,Y2-1)==-1 || (!czy_krol(plansza(y-8,Plansza)) || x==y-8) ) &&
		(przelicz(Y1-1,Y2+1)==-1 || (!czy_krol(plansza(y+7,Plansza)) || x==y+7) )&&
		(przelicz(Y1-1,Y2-1)==-1 || (!czy_krol(plansza(y-9,Plansza)) || x==y-9) ) )
		{
		return true;
		}
		else if( y == przelicz('g',1) && roszada(y,SPRAWDZ,Plansza) && plansza(przelicz('f',1),Plansza)==PUSTE && plansza(przelicz('g',1),Plansza)==PUSTE)
		{
			roszada(0,ZROB,Plansza);
			return true;
		}
		else if( y == przelicz('c',1) && roszada(y,SPRAWDZ,Plansza) && plansza(przelicz('b',1),Plansza)==PUSTE && plansza(przelicz('c',1),Plansza)==PUSTE && plansza(przelicz('d',1),Plansza)==PUSTE)
		{
			roszada(0,ZROB,Plansza);
			return true;
		}
		else if( y == przelicz('g',8) && roszada(y,SPRAWDZ,Plansza) && plansza(przelicz('f',8),Plansza)==PUSTE && plansza(przelicz('g',8),Plansza)==PUSTE)
		{
			roszada(0,ZROB,Plansza);
			return true;
		}
		else if( y == przelicz('c',8) && roszada(y,SPRAWDZ,Plansza) && plansza(przelicz('b',8),Plansza)==PUSTE && plansza(przelicz('c',8),Plansza)==PUSTE && plansza(przelicz('d',8),Plansza)==PUSTE)
		{
			roszada(0,ZROB,Plansza);
			return true;
		}

	return false;
}

bool czy_mat(PLANSZA Plansza,GRACZ Gracz)						
{
	int i,j;
	for(i=0;i<64;i++)
		if(czy_gracza(plansza(i,Plansza),Gracz))
			for(j=0;j<64;j++)
				if(i==j)
					continue;
				else if(ruch(i,j,Plansza,RUCH,Gracz) && !czy_bedzie_szach(i,j,Plansza,Gracz))
					return false;
	return true;
}		

bool czy_pat(PLANSZA Plansza,GRACZ Gracz)
{
	if(czy_mat(Plansza,Gracz))
		return true;
	return false;
}

bool czy_krol(POLE Pole)								
{
	if(Pole == KROLb || Pole == KROLc)
		return true;
	return false;
}