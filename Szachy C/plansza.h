#ifndef FIGURCZAK
#define FIGURCZAK
class figura;
typedef enum  pole { PIONEKb='P', WIEZAb='R', GONIECb='B', SKOCZEKb='N', KROLb='K', KROLOWAb='Q', 
			PIONEKc='p', WIEZAc='r', GONIECc='b', SKOCZEKc='n', KROLc='k', KROLOWAc='q', PUSTE='X' } POLE;
typedef enum stan { STOP, RUCH, SZACH, MAT, PAT } STAN;
typedef enum gracz { BIALE, CZARNE } GRACZ;
typedef enum tryp { WIRTUALNY, RZECZYWISTY } TRYB;
typedef enum tryb_roszada { UAKTUALNIJ, SPRAWDZ, ZROB , CZY_MOZNA, ZROBIONA } TRYB_ROSZADA;
typedef class figura * PLAN;
typedef class figura ** PLANSZA;

bool tura(int *,int *,PLANSZA ,STAN *,GRACZ *);			
bool ruch(int ,int ,PLANSZA ,STAN ,GRACZ);
bool ruch_pionek(int,int,PLANSZA ,GRACZ);
bool ruch_wieza(int,int,PLANSZA ,GRACZ);
bool ruch_goniec(int,int,PLANSZA ,GRACZ);
bool ruch_skoczek(int,int,PLANSZA ,GRACZ);
bool ruch_krol(int,int,PLANSZA ,GRACZ); 
bool ruch_krolowa(int,int,PLANSZA ,GRACZ);
void rusz_sie(int ,int ,PLANSZA ,TRYB );
bool ruch_figury(int,int,PLANSZA ,GRACZ);
bool roszada(int,TRYB_ROSZADA,PLANSZA );

POLE pole(int, POLE *);

class figura
{	
	public:
		POLE symbol;
		int pozycja;
		GRACZ gracz;
		virtual bool czy_ruszone(void) {return false;}
		virtual void poruszone(bool) {;}
		virtual void skok(void) {;}
		virtual bool ruch_figura(int , int , PLANSZA , GRACZ ) { return false; }
		static int tura;
		static bool roszadaa;

		figura(int npozycja,GRACZ ngracz) { pozycja=npozycja; gracz=ngracz; } 
};



class pionek : public figura
{
	public:
	bool ruch_figura(int x, int y, PLANSZA Plansza, GRACZ Gracz) { return ruch_pionek(x,y,Plansza,Gracz); }
	pionek(int npozycja,GRACZ ngracz) : figura(npozycja,ngracz) { symbol = (gracz == BIALE) ? PIONEKb : PIONEKc; }
};

class wieza : public figura
{
	public:
	bool ruszone;
	void poruszone(bool x) { ruszone=x; }
	bool czy_ruszone(void) { return ruszone; }
	bool ruch_figura(int x, int y, PLANSZA Plansza, GRACZ Gracz) { return ruch_wieza(x,y,Plansza,Gracz); }
	wieza(int npozycja,GRACZ ngracz) : figura(npozycja,ngracz) { symbol = (gracz == BIALE) ? WIEZAb : WIEZAc;
																	ruszone=true;}
};

class skoczek : public figura
{
	public:
	bool ruch_figura(int x, int y, PLANSZA Plansza, GRACZ Gracz) { return ruch_skoczek(x,y,Plansza,Gracz); }
	skoczek(int npozycja,GRACZ ngracz) : figura(npozycja,ngracz) { symbol = (gracz == BIALE) ? SKOCZEKb : SKOCZEKc; }
};
class goniec : public figura
{
	public:
	bool ruch_figura(int x, int y, PLANSZA Plansza, GRACZ Gracz) { return ruch_goniec(x,y,Plansza,Gracz); }
	goniec(int npozycja,GRACZ ngracz) : figura(npozycja,ngracz) { symbol = (gracz == BIALE) ? GONIECb : GONIECc; }
};

class krol : public figura
{
	public:
	bool ruszone;
	bool czy_ruszone(void) { return ruszone; }
	void poruszone(bool x) { ruszone=x; }
	bool ruch_figura(int x, int y, PLANSZA Plansza, GRACZ Gracz){ return ruch_krol(x,y,Plansza,Gracz); }
	krol(int npozycja,GRACZ ngracz) : figura(npozycja,ngracz) { symbol = (gracz == BIALE) ? KROLb : KROLc; 
																	ruszone=true;}
};

class krolowa : public figura
{
	public:
	bool ruch_figura(int x, int y, PLANSZA Plansza, GRACZ Gracz) { return ruch_krolowa(x,y,Plansza,Gracz); }
	krolowa(int npozycja,GRACZ ngracz) : figura(npozycja,ngracz) { symbol = (gracz == BIALE) ? KROLOWAb : KROLOWAc; }
};


POLE plansza(int, PLANSZA ); 
int przelicz(int , int );
void usun();
int getx();
int gety();
int geta();
int getm();
void usun_plansze(PLANSZA *);

void pobiez_ruch(int *,int *);			
void pobierzxy(int *);

void start_gry(STAN *);
void koniec_gry(STAN *);
bool start(STAN *);
bool koniec(STAN *);				
void inicjuj_plansze(PLANSZA *);			
void kopiuj_plansze(PLANSZA ,const PLANSZA );

bool rysuj_gre(PLANSZA , STAN *,GRACZ *); 	
void pokaz_plansze(PLANSZA ,GRACZ);		




bool czy_bedzie_szach(int,int,PLANSZA ,GRACZ);
bool czy_szach(PLANSZA ,GRACZ);
bool czy_mat(PLANSZA ,GRACZ);		
bool czy_pat(PLANSZA ,GRACZ);		
bool czy_puste(POLE);
bool czy_biale(POLE);
bool czy_czarne(POLE);
bool czy_gracza(POLE,GRACZ);
bool czy_krol(POLE);
#endif