#ifndef SZACHY
#define SZACHY

#include<vector>
using namespace std;
#define H8 (0)
#define A1 (1)
#define H1 (2)
#define A8 (3)
#define E1 (4)
#define E8 (5)
#define DO (6)

typedef enum  pole { PIONEKb='P', WIEZAb='R', GONIECb='B', SKOCZEKb='N', KROLb='K', KROLOWAb='Q', 
			PIONEKc='p', WIEZAc='r', GONIECc='b', SKOCZEKc='n', KROLc='k', KROLOWAc='q', PUSTE='X' } POLE;
typedef enum stan { STOP, RUCH, SZACH, MAT, PAT } STAN;
typedef enum gracz { BIALE, CZARNE } GRACZ;
typedef enum tryp { WIRTUALNY, RZECZYWISTY } TRYB;
typedef enum tryb_roszada { UAKTUALNIJ, SPRAWDZ, ZROB , CZY_MOZNA, ZROBIONA, SKOK } TRYB_SPECJALNY;
typedef struct {
				POLE szachownica[64];
				bool roszada[7];
				int skok[3];
				int tura;
				}  PLAN;
typedef PLAN * PLANSZA;
POLE plansza(int, PLANSZA ); 
int przelicz(int , int );
void usun();
int getx();
int gety();
int geta();
int getm();
int getp();
void usun_plansze(PLANSZA *);

void pobiez_ruch(int *,int *);			
void pobierzxy(int *);

void start_gry(STAN *);
void koniec_gry(STAN *);
bool start(STAN *);
bool koniec(STAN *);				
void inicjuj_plansze(PLANSZA *);			
void kopiuj_plansze(PLANSZA ,const PLANSZA );

bool rysuj_gre(PLANSZA , STAN *,GRACZ *); 	
void pokaz_plansze(PLANSZA ,GRACZ);		

bool tura(int *,int *,PLANSZA ,STAN *,GRACZ *);			
bool ruch(int ,int ,PLANSZA ,STAN ,GRACZ);
bool ruch_pionek(int,int,PLANSZA ,GRACZ);
bool ruch_wieza(int,int,PLANSZA ,GRACZ);
bool ruch_goniec(int,int,PLANSZA ,GRACZ);
bool ruch_skoczek(int,int,PLANSZA ,GRACZ);
bool ruch_krol(int,int,PLANSZA ,GRACZ); 
bool ruch_krolowa(int,int,PLANSZA ,GRACZ);
void rusz_sie(int ,int ,PLANSZA ,TRYB );
bool ruch_figury(int,int,PLANSZA ,GRACZ);
bool roszada(int,TRYB_SPECJALNY,PLANSZA );
bool bicie_w_przelocie(int,TRYB_SPECJALNY,PLANSZA );


bool czy_bedzie_szach(int,int,PLANSZA ,GRACZ);
bool czy_szach(PLANSZA ,GRACZ);
bool czy_mat(PLANSZA ,GRACZ);		
bool czy_pat(PLANSZA ,GRACZ);		
bool czy_puste(POLE);
bool czy_biale(POLE);
bool czy_czarne(POLE);
bool czy_gracza(POLE,GRACZ);
bool czy_krol(POLE);

class Node;
int ocen_plansze(PLANSZA Plansza,GRACZ Gracz,int *waga1, int *waga2,int *waga3,int *waga4);
int ocen_pole(POLE Pole);
bool rob_drzewo(PLANSZA Plansza,GRACZ Gracz,int,Node *);
void komputer(PLANSZA Plansza,GRACZ * Gracz,TRYB,STAN *,int = 100,int =1,int =5,int =10);
void usun_drzewo(Node *root,unsigned n);
void ocen_drzewo(Node *root, GRACZ Gracz_aktualny, GRACZ Gracz,unsigned n);
void generuj_pionek(vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz,int x);
void generuj_wieza(vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz,int x);
void generuj_goniec(vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz,int x);
void generuj_krolowa(vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz,int x);
void generuj_skoczek(vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz,int x);
void generuj_krol(vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz,int x);
void gen_ruch(int i,vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz);
void generuj_ruszek(vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz);
#endif