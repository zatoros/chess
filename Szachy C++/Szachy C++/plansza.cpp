//Plik z funkcjami do planszy
#include<stdio.h>
#include<vector>
#include"plansza.h"
using namespace std;

POLE plansza(int y,PLANSZA  Plansza)
{
	return Plansza->szachownica[y];
}

void inicjuj_plansze(PLANSZA * Plansza)												
{
	int i;
		(*Plansza) = new PLAN;					
		(*Plansza)->szachownica[przelicz('a',1)]=(*Plansza)->szachownica[przelicz('h',1)]=WIEZAb;
		(*Plansza)->szachownica[przelicz('a',8)]=(*Plansza)->szachownica[przelicz('h',8)]=WIEZAc;
		(*Plansza)->szachownica[przelicz('b',1)]=(*Plansza)->szachownica[przelicz('g',1)]=SKOCZEKb;
		(*Plansza)->szachownica[przelicz('b',8)]=(*Plansza)->szachownica[przelicz('g',8)]=SKOCZEKc;
		(*Plansza)->szachownica[przelicz('c',1)]=(*Plansza)->szachownica[przelicz('f',1)]=GONIECb;
		(*Plansza)->szachownica[przelicz('c',8)]=(*Plansza)->szachownica[przelicz('f',8)]=GONIECc;
		(*Plansza)->szachownica[przelicz('e',1)]=KROLb;
		(*Plansza)->szachownica[przelicz('e',8)]=KROLc;
		(*Plansza)->szachownica[przelicz('d',1)]=KROLOWAb;
		(*Plansza)->szachownica[przelicz('d',8)]=KROLOWAc;
		
		for(i=przelicz('a',2);i<=przelicz('h',2);i++)
			(*Plansza)->szachownica[i]=PIONEKb;

		for(i=przelicz('a',7);i<=przelicz('h',7);i++)
			(*Plansza)->szachownica[i]=PIONEKc;		

		for(i=przelicz('a',3);i<=przelicz('h',6);i++)
			(*Plansza)->szachownica[i]=PUSTE;				
			
		for(i=0;i<6;i++)
			(*Plansza)->roszada[i]=true;
			(*Plansza)->roszada[i]=false;
		for(i=0;i<3;i++)
			(*Plansza)->skok[i]=0;
}


void kopiuj_plansze(PLANSZA  Kopia, const PLANSZA  Plansza)
{
	int i;
		for(i=0;i<64;i++)
			Kopia->szachownica[i]=Plansza->szachownica[i];
		for(i=0;i<6;i++)
		Kopia->roszada[i]=Plansza->roszada[i];
}

void rusz_sie( int x , int y,PLANSZA Pole,TRYB tryb)					
{	
	Pole->szachownica[y]=Pole->szachownica[x];
	Pole->szachownica[x]=PUSTE;
	
			if(tryb==RZECZYWISTY)
				roszada(x,UAKTUALNIJ,Pole);
			if(tryb==RZECZYWISTY && ( (Pole->szachownica[y]==PIONEKb && ( y>=przelicz('a',8) && y<=przelicz('h',8)) ) || 
				(Pole->szachownica[y]==PIONEKc && ( y>=przelicz('a',1) && y<=przelicz('h',1)) ) ) )
			{
				int awans;
				printf("Na co chcesz awansowac pionka??\n");
				printf("1)SKOCZEK | 2)GONIEC | 3)WIEZA | 4)KROLOWA\n");
				awans=geta();
				switch(awans)
				{
					case 1:
						Pole->szachownica[y]= (Pole->szachownica[y] == PIONEKb ? SKOCZEKb : SKOCZEKc);
						break;
					case 2:
						Pole->szachownica[y]= (Pole->szachownica[y] == PIONEKb ? GONIECb : GONIECc);
						break;
					case 3:
						Pole->szachownica[y]= (Pole->szachownica[y] == PIONEKb ? WIEZAb : WIEZAc);
						break;
					case 4:
						Pole->szachownica[y]= (Pole->szachownica[y] == PIONEKb ? KROLOWAb : KROLOWAc);
						break;
				}
			}
			else if (roszada(0,CZY_MOZNA,Pole))
			{
			if(tryb == RZECZYWISTY)
				roszada(0,ZROBIONA,Pole);
			if(y == przelicz('g',1) )
					rusz_sie(przelicz('h',1),przelicz('f',1),Pole,WIRTUALNY);
			else if(y == przelicz('c',1) )
					rusz_sie(przelicz('a',1),przelicz('d',1),Pole,WIRTUALNY);
			else if(y == przelicz('g',8) )
					rusz_sie(przelicz('h',8),przelicz('f',8),Pole,WIRTUALNY);
			else if(y == przelicz('c',8) )
					rusz_sie(przelicz('a',8),przelicz('d',8),Pole,WIRTUALNY);
			}
			else if (bicie_w_przelocie(0,CZY_MOZNA,Pole))
			{
				if(tryb == RZECZYWISTY)
					bicie_w_przelocie(0,ZROBIONA,Pole);
				Pole->szachownica[y+8]=Pole->szachownica[y-8]=PUSTE;
			}
				
}

bool roszada(int y, TRYB_SPECJALNY tryb,PLANSZA Plansza)
{

			if(tryb == UAKTUALNIJ )
				{
					if(y == przelicz('h',8))
						Plansza->roszada[H8] = false;
					else if( y == przelicz('a',1))
						Plansza->roszada[A1] = false;
					else if( y == przelicz('h',1))
						Plansza->roszada[H1] = false;
					else if( y == przelicz('a',8))
						Plansza->roszada[A8] = false;
					else if( y == przelicz('e',1))
						Plansza->roszada[E1] = false;
					else if( y == przelicz('e',8))
						Plansza->roszada[E8] = false;
						
					return false;
				}
			else if(tryb == SPRAWDZ )
				{
					if( y == przelicz('g',1) && Plansza->roszada[E1] && Plansza->roszada[H1])
							return true;
					else if ( y == przelicz('c',1) && Plansza->roszada[E1] && Plansza->roszada[A1])
							return true;
					else if ( y == przelicz('g',8) && Plansza->roszada[E8] && Plansza->roszada[H8])
							return true;
					else if( y == przelicz('c',8) && Plansza->roszada[E8] && Plansza->roszada[A8])
							return true;
					else 
							return false;
				}
			else if(tryb ==ZROB )
				Plansza->roszada[DO] = true;
			else if(tryb == CZY_MOZNA )
				return Plansza->roszada[DO];
			else if(tryb == ZROBIONA )
				Plansza->roszada[DO] = false;
	return false;		
}

bool bicie_w_przelocie(int y, TRYB_SPECJALNY tryb,PLANSZA Plansza)
{
	if(tryb == SKOK)
	{
		Plansza->skok[0]=1;
		Plansza->skok[1]=y;
	}
	else if( tryb == UAKTUALNIJ && Plansza->skok[0] == 1)
		Plansza->skok[0]++;
	else if( tryb == UAKTUALNIJ && Plansza->skok[0] == 2)
	{
		Plansza->skok[1]=-1;
		Plansza->skok[0]=0;
	}
	else if(tryb == SPRAWDZ)
	{
		if(Plansza->skok[1] == y)
			return true;
	}
	else if(tryb == ZROB)
		Plansza->skok[2] = 1;
	else if(tryb == CZY_MOZNA)
	{
		if(Plansza->skok[2]==1)
			return true;
		else 
			return false;
	}
	else if(tryb == ZROBIONA)
		Plansza->skok[2] = 0;

	return false;
}

bool ruch_figury(int x, int y, PLANSZA Plansza, GRACZ Gracz)	
{
		switch(plansza(x,Plansza))										
	{
		case PIONEKb :
		case PIONEKc :
						return ruch_pionek(x,y,Plansza,Gracz);    
		case WIEZAb :
		case WIEZAc :
						return ruch_wieza(x,y,Plansza,Gracz);				
		case GONIECb :
		case GONIECc :
						return ruch_goniec(x,y,Plansza,Gracz);			
		case SKOCZEKb :
		case SKOCZEKc :
						return ruch_skoczek(x,y,Plansza,Gracz);			
		case KROLb :
		case KROLc :
						return ruch_krol(x,y,Plansza,Gracz);			
		case KROLOWAb :
		case KROLOWAc :
						return ruch_krolowa(x,y,Plansza,Gracz);	
						
	}

}

void usun_plansze(PLANSZA *Plansza)
{
	delete *Plansza;
}

void gen_ruch(int i,vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz)
{
	switch(plansza(i,Plansza))										
	{
		case PIONEKb :
		case PIONEKc :
					 generuj_pionek(tab,tap,Plansza,Gracz,i);
					 break;
		case WIEZAb :
		case WIEZAc :
					 generuj_wieza(tab,tap,Plansza,Gracz,i);
					 break;			
		case GONIECb :
		case GONIECc :
					 generuj_goniec(tab,tap,Plansza,Gracz,i);
					 break;	
		case SKOCZEKb :
		case SKOCZEKc :
					 generuj_skoczek(tab,tap,Plansza,Gracz,i);
					 break;		
		case KROLb :
		case KROLc :
					 generuj_krol(tab,tap,Plansza,Gracz,i);
					 break;			
		case KROLOWAb :
		case KROLOWAc :
					 generuj_krolowa(tab,tap,Plansza,Gracz,i);
					 break;
						
	}
}