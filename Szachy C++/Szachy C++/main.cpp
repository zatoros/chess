//plik glowny
//										INSTRUKCJA:
//JESLI CHCEMY ZEBY REPREZENTACJA DANYCH BYLY OBIEKTY TO NALEZY:
//1) DOLACZYC NAGLOWEK "figury.h" do plikow main.cpp oraz szachy.cpp
//2) SKOMPILOWAC PLIK main.cpp z plikami szachy.cpp oraz figury.cpp
//JESLI CHCEMY ABY REPREZENTACJA BYLA STRUKTURA TO NALEZY:
//1) TAK JAK DO FIGUR ALE NAGLOWEK "plansza.h"
//2) SKOMPILOWAC PLIK main.cpp z plikami szachy.cpp i plansza.cpp

			//GRA Z KOMPUTEREM//
///////////////////////////////////////////////////////////////////////
///LATWE : PODPOWIEDZI///////////////////////////////
///SREDNIE: POGLEBIANIE DRZEWA///////////////////////
///TRUDNE: LEPSZE GENEROWANIE RUCHOW, UCZENIE SIE////
////////////////////////////////////////////////////
#include<stdio.h>
#include"plansza.h"
#include<iostream>

int main(void)
{
int to[2], from[2];
PLANSZA Plansza;
STAN Stan = STOP;
GRACZ Gracz = BIALE;
int menu, podp;


	inicjuj_plansze(&Plansza);
	do
	{
		printf("\tSZACHY\n");
		pokaz_plansze(Plansza,Gracz);
		printf("1)GRACZ VS GRACZ | 2)KOMPUTER VS GRACZ | 3)KONIEC 4) UCZ SIE:)\n");
		menu=getm();
		printf("Chcesz grac z podpowiedziami??\n");
		printf("1) TAK | 2) NIE\n");
		podp=getp();
		switch(menu)
		{
			case 1:
				start_gry(&Stan);
				usun_plansze(&Plansza);
				inicjuj_plansze(&Plansza);

					for( ; ;)
					{
						rysuj_gre(Plansza,&Stan,&Gracz);
						if(Stan == MAT || Stan == PAT)
							break;
						else
						{
							if( podp == 1)
								komputer(Plansza,&Gracz,WIRTUALNY,&Stan);
							pobiez_ruch(from,to);
							if(!tura(from,to,Plansza,&Stan,&Gracz))
								printf("Sproboj jeszcze raz\n");
						}
					}
	
				koniec_gry(&Stan);
			break;
			case 2:
				start_gry(&Stan);
				usun_plansze(&Plansza);
				inicjuj_plansze(&Plansza);

					for( ; ;)
					{
						rysuj_gre(Plansza,&Stan,&Gracz);
						if(Stan == MAT || Stan == PAT)
							break;
						else
						{
							if(Gracz == BIALE)
								komputer(Plansza,&Gracz,RZECZYWISTY,&Stan);
							else
							{
							if( podp == 1)
								komputer(Plansza,&Gracz,WIRTUALNY,&Stan);
							pobiez_ruch(from,to);
							if(!tura(from,to,Plansza,&Stan,&Gracz))
								printf("Sproboj jeszcze raz\n");
							}
						}
					}
	
				koniec_gry(&Stan);
				break;
			case 4:
				start_gry(&Stan);
				usun_plansze(&Plansza);
				inicjuj_plansze(&Plansza);
				int waga[2][4] = { {100,1,5,10}, {60,50,40,30}};
					for( ; ;)
					{
						rysuj_gre(Plansza,&Stan,&Gracz);
						if(Stan == MAT || Stan == PAT)
							break;
						else
						{
							if(Gracz == BIALE)
								komputer(Plansza,&Gracz,RZECZYWISTY,&Stan);
							else
							{
								komputer(Plansza,&Gracz,RZECZYWISTY,&Stan,60,50,40,30);
							}
						}
					}
					printf("Wygraly wagi: ");
						for(int i =0;i<4;i++)
							printf("%d ",waga[Gracz][i]);
						putchar('\n');
				koniec_gry(&Stan);
				break;
		}
	}
	while(menu!=3);
	
return 0;
}