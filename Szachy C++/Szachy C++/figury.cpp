//Plik z funkcjami do figur
/*#include<stdio.h>
#include"figury.h"
bool figura::roszadaa = false;
bool figura::przelot = false;
int figura::tura = 0;

POLE plansza(int y,PLANSZA  Plansza)
{
	int i;
		for(i=0;i<32;i++)
			if(Plansza[i]->pozycja == y)
				return Plansza[i]->symbol;
	return PUSTE;
}

void inicjuj_plansze(PLANSZA * Plansza)												
{
	int i,j;
			(*Plansza) = new PLAN[32];
		(*Plansza)[0] = new wieza(przelicz('a',1),BIALE);
		(*Plansza)[1] = new wieza(przelicz('h',1),BIALE);
		(*Plansza)[2] = new wieza(przelicz('a',8),CZARNE);
		(*Plansza)[3] = new wieza(przelicz('h',8),CZARNE);
		(*Plansza)[4] = new skoczek(przelicz('b',1),BIALE);
		(*Plansza)[5] = new skoczek(przelicz('g',1),BIALE);
		(*Plansza)[6] = new skoczek(przelicz('b',8),CZARNE);
		(*Plansza)[7] = new skoczek(przelicz('g',8),CZARNE);
		(*Plansza)[8] = new goniec(przelicz('c',1),BIALE);
		(*Plansza)[9] = new goniec(przelicz('f',1),BIALE);
		(*Plansza)[10] = new goniec(przelicz('c',8),CZARNE);
		(*Plansza)[11] = new goniec(przelicz('f',8),CZARNE);
		(*Plansza)[12] = new krol(przelicz('e',1),BIALE);
		(*Plansza)[13] = new krol(przelicz('e',8),CZARNE);
		(*Plansza)[14] = new krolowa(przelicz('d',1),BIALE);
		(*Plansza)[15] = new krolowa(przelicz('d',8),CZARNE);

		
		for(j=16,i=przelicz('a',2);i<=przelicz('h',2);i++,j++)
			(*Plansza)[j] = new pionek(i,BIALE);

		for(j=24,i=przelicz('a',7);i<=przelicz('h',7);i++,j++)
			(*Plansza)[j]=new pionek(i,CZARNE);		
			
}

void kopiuj_plansze(PLANSZA  Kopia, const PLANSZA  Plansza)
{
	int i;
	for(i=0;i<16;i++)
	{
		Kopia[i]->pozycja=Plansza[i]->pozycja;
		Kopia[i]->symbol=Plansza[i]->symbol;
		Kopia[i]->gracz=Plansza[i]->gracz;
	}

	for(i=16;i<32;i++)
	{
		if(Plansza[i]->symbol== PIONEKb || Plansza[i]->symbol== PIONEKc)
		{
			Kopia[i]->pozycja=Plansza[i]->pozycja;
			Kopia[i]->symbol=Plansza[i]->symbol;
			Kopia[i]->gracz=Plansza[i]->gracz;
		}
	
		else
		{
			GRACZ gracz=Plansza[i]->gracz;
				delete Kopia[i];
			switch(Plansza[i]->symbol)
			{
				case WIEZAb :
				case WIEZAc :
						Kopia[i] = new wieza(Plansza[i]->pozycja,gracz);				
				case GONIECb :
				case GONIECc :
						Kopia[i] = new goniec(Plansza[i]->pozycja,gracz);			
				case SKOCZEKb :
				case SKOCZEKc :
						Kopia[i] = new skoczek(Plansza[i]->pozycja,gracz);							
				case KROLOWAb :
				case KROLOWAc :
						Kopia[i] = new krolowa(Plansza[i]->pozycja,gracz);	
			}
		}
	}
	

}

void rusz_sie( int x , int y,PLANSZA Pole,TRYB tryb)
{
	int k;
	GRACZ gracz;
	for(int i=0;i<32;i++)
	{
		if(Pole[i]->pozycja == y)
			Pole[i]->pozycja = -1;
		if(Pole[i]->pozycja == x)
		{
			Pole[i]->pozycja =y;
			k=i;
		}
	}
	if(tryb == RZECZYWISTY)
		roszada(x,UAKTUALNIJ,Pole);
	
	if(tryb==RZECZYWISTY && ( (Pole[k]->symbol==PIONEKb && ( y>=przelicz('a',8) && y<=przelicz('h',8)) ) || 
				(Pole[k]->symbol==PIONEKc && ( y>=przelicz('a',1) && y<=przelicz('h',1)) ) ) )
			{
				int awans;
				printf("Na co chcesz awansowac pionka??\n");
				printf("1)SKOCZEK | 2)GONIEC | 3)WIEZA | 4)KROLOWA\n");
				awans=geta();
				gracz=Pole[k]->gracz;
				delete Pole[k];
				switch(awans)
				{
					case 1:
						Pole[k] = new skoczek(y,gracz);
						break;
					case 2:
						Pole[k] = new goniec(y,gracz);
						break;
					case 3:
						Pole[k] = new wieza(y,gracz);
						break;
					case 4:
						Pole[k] = new krolowa(y,gracz);
						break;
				}
			}
			else if (roszada(0,CZY_MOZNA,Pole))
			{
			if(tryb == RZECZYWISTY)
				roszada(0,ZROBIONA,Pole);
			if(y == przelicz('g',1) )
					rusz_sie(przelicz('h',1),przelicz('f',1),Pole,WIRTUALNY);
			else if(y == przelicz('c',1) )
					rusz_sie(przelicz('a',1),przelicz('d',1),Pole,WIRTUALNY);
			else if(y == przelicz('g',8) )
					rusz_sie(przelicz('h',8),przelicz('f',8),Pole,WIRTUALNY);
			else if(y == przelicz('c',8) )
					rusz_sie(przelicz('a',8),przelicz('d',8),Pole,WIRTUALNY);
			}
			else if(bicie_w_przelocie(0,CZY_MOZNA,Pole))
			{
				if(tryb == RZECZYWISTY)
					roszada(0,ZROBIONA,Pole);
					
				for(int i=0;i<32;i++)
					{
						if(Pole[i]->pozycja == y+8 || Pole[i]->pozycja== y-8)
								Pole[i]->pozycja = -1;
					}
			}
}


bool roszada(int y, TRYB_SPECJALNY tryb,PLANSZA  Plansza)
{

			if(tryb == UAKTUALNIJ )
				{
					if(y == przelicz('h',8))
						Plansza[3]->poruszone(false);
					else if( y == przelicz('a',1))
						Plansza[0]->poruszone(false);
					else if( y == przelicz('h',1))
						Plansza[1]->poruszone(false);
					else if( y == przelicz('a',8))
						Plansza[2]->poruszone(false);
					else if( y == przelicz('e',1))
						Plansza[12]->poruszone(false);
					else if( y == przelicz('e',8))
						Plansza[13]->poruszone(false);
						
					return false;
				}
			else if(tryb == SPRAWDZ )
				{
					if( y == przelicz('g',1) && Plansza[12]->czy_ruszone() && Plansza[0]->czy_ruszone())
							return true;
					else if ( y == przelicz('c',1) && Plansza[12]->czy_ruszone() && Plansza[1]->czy_ruszone())
							return true;
					else if ( y == przelicz('g',8) && Plansza[13]->czy_ruszone() && Plansza[3]->czy_ruszone())
							return true;
					else if( y == przelicz('c',8) && Plansza[13]->czy_ruszone() && Plansza[2]->czy_ruszone())
							return true;
					else 
							return false;
				}
			else if(tryb ==ZROB )
				figura::roszadaa = true;
			else if(tryb == CZY_MOZNA )
				return figura::roszadaa;
			else if(tryb == ZROBIONA )
				figura::roszadaa = false;
	return false;		
}

bool bicie_w_przelocie(int y, TRYB_SPECJALNY tryb,PLANSZA Plansza) 
{
	if(tryb == SKOK)
	{
		for(int i=0;i<32;i++)
			if(Plansza[i]->pozycja == y-8 || Plansza[i]->pozycja == y+8)
			{ 
				Plansza[i]->skok(y); 
				figura::tura=1;
				break;
			}
	}
	else if( tryb == UAKTUALNIJ && figura::tura == 1)
		figura::tura++;
	else if( tryb == UAKTUALNIJ && figura::tura == 2 )
	{
		figura::tura=0;
		for(int i=16;i<32;i++)
			if(Plansza[i]->Pionek_widmo() != -1)
			Plansza[i]->skok(-1);
	}
	else if(tryb == SPRAWDZ)
	{
		for(int i=16;i<32;i++)
		if(Plansza[i]->Pionek_widmo() == y)
			return true;
	}
	else if(tryb == ZROB)
		figura::przelot = true;
	else if(tryb == CZY_MOZNA)
		return figura::przelot;
	
	else if(tryb == ZROBIONA)
		figura::przelot = false;

	return false;
}

bool ruch_figury(int x, int y, PLANSZA  Plansza, GRACZ Gracz)	
{
	int i;
		for(i=0;i<32;i++)
			if(Plansza[i]->pozycja == x)
				return Plansza[i]->ruch_figura(x,y,Plansza,Gracz);
}

void usun_plansze(PLANSZA *Plansza)
{
	for(int i=0;i<32;i++)
		delete (*Plansza)[i];
	delete[] (*Plansza);
}

void gen_ruch(int i,vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz)
{
		int x;
		for(x=0;x<32;x++)
			if(Plansza[x]->pozycja == i)
				Plansza[x]->generuj(i,tab,tap,Plansza,Gracz);
}*/