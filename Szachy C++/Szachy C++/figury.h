#ifndef FIGURCZAK
#define FIGURCZAK
#include<vector>
using namespace std;

class figura;
typedef enum  pole { PIONEKb='P', WIEZAb='R', GONIECb='B', SKOCZEKb='N', KROLb='K', KROLOWAb='Q', 
			PIONEKc='p', WIEZAc='r', GONIECc='b', SKOCZEKc='n', KROLc='k', KROLOWAc='q', PUSTE='X' } POLE;
typedef enum stan { STOP, RUCH, SZACH, MAT, PAT } STAN;
typedef enum gracz { BIALE, CZARNE } GRACZ;
typedef enum tryp { WIRTUALNY, RZECZYWISTY } TRYB;
typedef enum tryb_roszada { UAKTUALNIJ, SPRAWDZ, ZROB , CZY_MOZNA, ZROBIONA, SKOK } TRYB_SPECJALNY;
typedef class figura * PLAN;
typedef class figura ** PLANSZA;

void generuj_pionek(vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz,int x);
void generuj_wieza(vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz,int x);
void generuj_goniec(vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz,int x);
void generuj_krolowa(vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz,int x);
void generuj_skoczek(vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz,int x);
void generuj_krol(vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz,int x);

bool tura(int *,int *,PLANSZA ,STAN *,GRACZ *);			
bool ruch(int ,int ,PLANSZA ,STAN ,GRACZ);
bool ruch_pionek(int,int,PLANSZA ,GRACZ);
bool ruch_wieza(int,int,PLANSZA ,GRACZ);
bool ruch_goniec(int,int,PLANSZA ,GRACZ);
bool ruch_skoczek(int,int,PLANSZA ,GRACZ);
bool ruch_krol(int,int,PLANSZA ,GRACZ); 
bool ruch_krolowa(int,int,PLANSZA ,GRACZ);
void rusz_sie(int ,int ,PLANSZA ,TRYB );
bool ruch_figury(int,int,PLANSZA ,GRACZ);
bool roszada(int,TRYB_SPECJALNY,PLANSZA );
bool bicie_w_przelocie(int,TRYB_SPECJALNY,PLANSZA );

POLE pole(int, POLE *);

class figura
{	
	public:
		POLE symbol;
		int pozycja;
		GRACZ gracz;
		virtual bool czy_ruszone(void) {return false;}
		virtual void poruszone(bool) {;}
		virtual void skok(int) {;}
		virtual int Pionek_widmo(void) { return 0; }
		virtual bool ruch_figura(int , int , PLANSZA , GRACZ ) { return false; }
		static bool przelot;
		static bool roszadaa;
		static int tura;
		virtual void generuj(int i,vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz) { ;}

		figura(int npozycja,GRACZ ngracz) { pozycja=npozycja; gracz=ngracz; } 
};



class pionek : public figura
{
	public:
	int pionek_widmo;
	void skok(int x) { pionek_widmo=x; }
	int Pionek_widmo(void) { return pionek_widmo; }
	bool ruch_figura(int x, int y, PLANSZA Plansza, GRACZ Gracz) { return ruch_pionek(x,y,Plansza,Gracz); }
	pionek(int npozycja,GRACZ ngracz) : figura(npozycja,ngracz) { symbol = (gracz == BIALE) ? PIONEKb : PIONEKc; 
																	pionek_widmo=-1;}
	void generuj(int i,vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz) { ;} 
};

class wieza : public figura
{
	public:
	bool ruszone;
	void poruszone(bool x) { ruszone=x; }
	bool czy_ruszone(void) { return ruszone; }
	bool ruch_figura(int x, int y, PLANSZA Plansza, GRACZ Gracz) { return ruch_wieza(x,y,Plansza,Gracz); }
	wieza(int npozycja,GRACZ ngracz) : figura(npozycja,ngracz) { symbol = (gracz == BIALE) ? WIEZAb : WIEZAc;
																	ruszone=true;}
	void generuj(int i,vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz) { generuj_wieza(tab,tap,Plansza,Gracz,i); }
};

class skoczek : public figura
{
	public:
	bool ruch_figura(int x, int y, PLANSZA Plansza, GRACZ Gracz) { return ruch_skoczek(x,y,Plansza,Gracz); }
	skoczek(int npozycja,GRACZ ngracz) : figura(npozycja,ngracz) { symbol = (gracz == BIALE) ? SKOCZEKb : SKOCZEKc; }
	void generuj(int i,vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz) { generuj_skoczek(tab,tap,Plansza,Gracz,i); }
};
class goniec : public figura
{
	public:
	bool ruch_figura(int x, int y, PLANSZA Plansza, GRACZ Gracz) { return ruch_goniec(x,y,Plansza,Gracz); }
	goniec(int npozycja,GRACZ ngracz) : figura(npozycja,ngracz) { symbol = (gracz == BIALE) ? GONIECb : GONIECc; }
	void generuj(int i,vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz) { generuj_goniec(tab,tap,Plansza,Gracz,i); }
};

class krol : public figura
{
	public:
	bool ruszone;
	bool czy_ruszone(void) { return ruszone; }
	void poruszone(bool x) { ruszone=x; }
	bool ruch_figura(int x, int y, PLANSZA Plansza, GRACZ Gracz){ return ruch_krol(x,y,Plansza,Gracz); }
	krol(int npozycja,GRACZ ngracz) : figura(npozycja,ngracz) { symbol = (gracz == BIALE) ? KROLb : KROLc; 
																	ruszone=true;}
	void generuj(int i,vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz) { generuj_krol(tab,tap,Plansza,Gracz,i); }
};

class krolowa : public figura
{
	public:
	bool ruch_figura(int x, int y, PLANSZA Plansza, GRACZ Gracz) { return ruch_krolowa(x,y,Plansza,Gracz); }
	krolowa(int npozycja,GRACZ ngracz) : figura(npozycja,ngracz) { symbol = (gracz == BIALE) ? KROLOWAb : KROLOWAc; }
	void generuj(int i,vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz) { generuj_krolowa(tab,tap,Plansza,Gracz,i); }
};


POLE plansza(int, PLANSZA ); 
int przelicz(int , int );
void usun();
int getx();
int gety();
int geta();
int getm();
int getp();
void usun_plansze(PLANSZA *);

void pobiez_ruch(int *,int *);			
void pobierzxy(int *);

void start_gry(STAN *);
void koniec_gry(STAN *);
bool start(STAN *);
bool koniec(STAN *);				
void inicjuj_plansze(PLANSZA *);			
void kopiuj_plansze(PLANSZA ,const PLANSZA );

bool rysuj_gre(PLANSZA , STAN *,GRACZ *); 	
void pokaz_plansze(PLANSZA ,GRACZ);		




bool czy_bedzie_szach(int,int,PLANSZA ,GRACZ);
bool czy_szach(PLANSZA ,GRACZ);
bool czy_mat(PLANSZA ,GRACZ);		
bool czy_pat(PLANSZA ,GRACZ);		
bool czy_puste(POLE);
bool czy_biale(POLE);
bool czy_czarne(POLE);
bool czy_gracza(POLE,GRACZ);
bool czy_krol(POLE);

class Node;
int ocen_plansze(PLANSZA Plansza,GRACZ Gracz,int *waga1, int *waga2,int *waga3,int *waga4);
int ocen_pole(POLE Pole);
bool rob_drzewo(PLANSZA Plansza,GRACZ Gracz,int,Node *);
void komputer(PLANSZA Plansza,GRACZ * Gracz,TRYB,STAN *,int = 100,int =1,int =5,int =10);
void usun_drzewo(Node *root,unsigned n);
void ocen_drzewo(Node *root, GRACZ Gracz_aktualny, GRACZ Gracz,unsigned n);
void gen_ruch(int i,vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz);
void generuj_ruszek(vector<int> *tab,vector<int> *tap,PLANSZA Plansza,GRACZ Gracz);
#endif